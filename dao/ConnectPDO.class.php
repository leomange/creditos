<?php

/*******************************************************************************************************
CRIADO POR ALAN R.MOREIRA EM 05/04/2016

ESTA CLASSE RETONA A CONEXÃO COM OS BANCOS MySQL E MSSQL UTILIZANDO O PADRÃO SINGLETON.


###################################################################

SEGUE CÓDIGO PARA TESTE:

include_once 'Connect.class.php';

$conn = Connect::getInstance()->getConnection();

$result = $conn->query("SELECT * from uuid");

while($row = $result->fetch(PDO::FETCH_ASSOC)) 
{
	echo $row['uuid'].'<br/>';
}

###################################################################

/*******************************************************************************************************/
ini_set('mssql.charset', 'UTF-8');

class Connect{

	private static $instance; 
	private static $conn; 

	private function __construct(){

	}

	public static function getInstance(){

		if(!self::$instance instanceof self){

			self::$instance = new Connect();
		}

		return self::$instance;

	}

	public function getConnection(){

		try{

			/*if (file_exists('./dao/conexao.ini')) {

				$banco = parse_ini_file("./dao/conexao.ini");

			}else{

				throw new Exception("Arquivo conexao não encontrado ou URL incorreta");

			}
			
			
			
		
			$user = isset($banco['user']) ? $banco['user'] : NULL;
			$pass = isset($banco['pass']) ? $banco['pass'] : NULL;
			$name = isset($banco['name']) ? $banco['name'] : NULL;
			$host = isset($banco['host']) ? $banco['host'] : NULL;
			$type = isset($banco['type']) ? $banco['type'] : NULL;
			$port = isset($banco['port']) ? $banco['port'] : NULL;
		*/
		
		
			$host = 'bded-prd.positivo';
			$name = 'LivroDigital';
			$user = 'sisLivroDigital';
			$pass = 'CP9D9VS5';
			$type = 'mssql';
			
			switch ($type)
			{
				case 'mysql':
					$port = $port?$port:'3306';
					$this->conn = new PDO("mysql:host={$host};port={$port};dbname={$name}",$user,$pass);
					break;
				case 'mssql':

					$this->conn = new PDO("dblib:host={$host};dbname={$name};charset=UTF-8", $user, $pass);
					break;

			}
		
			$this->conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

		
			if($this->conn){

				return $this->conn;	

			}else{

				throw new ErrorException("getConnection: Erro na conexão Prd.",149);
				
			}

			
		}catch(ErrorException $e){
			
			echo SetErro($e->getCode()).'<br/>';
			echo SetMessage($e->getMessage()).'<br/>';
			echo SetFile($e->getFile()).'<br/>';
			echo SetLine($e->getLine()).'<br/>';
			echo SetTrace($e->getTraceAsString());

		}

	}

	
	
	
	
	
	// EXECUÇÃO DE QUERIES
	// Retorna um ARRAY associativo com o resultado da query
	/*
		EXEMPLO RESULTSET:
		
		nome			sexo			idade
		========================================
		Zé				M				40
		Juca			M				28
		Maria			F				21


		EXEMPLO ACESSO ARRAY FINAL $CM_dados:
		
		$CM_dados[0]['nome'] -> retorna "Zé"
		$CM_dados[2]['sexo'] -> retorna "F"
		$CM_dados[1]['idade'] -> retorna "28"
		
	*/
	public function execQuerySelect($query){
		$CM_dados = array();	
		try{		
			$conn = Connect::getInstance()->getConnection();
			//$result = $conn->query($query);
			
			// SQL Injection - 2º tratamento com prepare do PDO
			$result = $conn->prepare($query);
			$run = $result->execute();
			
			while($row = $result->fetch(PDO::FETCH_ASSOC)) 
			{
				array_push($CM_dados,$row);	
			}
			return($CM_dados);			
		}catch(ErrorException $e){
			return false;
		}
	}
	
	
	
	// EXECUÇÃO DE QUERIES DE INSERÇÃO
	// Retorna um ARRAY com o resultado da query - true ou false
	public function execQueryInsert($query){
		$CM_dados = array();	
		try{		
			$conn = Connect::getInstance()->getConnection();
			//$result = $conn->exec($query);
			
			// SQL Injection - 2º tratamento com prepare do PDO
			$result = $conn->prepare($query);
			$run = $result->execute();
			
			if ($result){
				$CM_dados[0]=true;
				$CM_dados[1]="";
			} else {
				$CM_dados[0]=false;
				$CM_dados[1]="Erro de banco de dados.";				
			}
		}catch(ErrorException $e){
			$CM_dados[0]=false;
			$CM_dados[1]=$e->getMessage();		
		}
		return($CM_dados);
	}
		
	
	


	
	
}