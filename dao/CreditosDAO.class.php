<?php

/**
 * API de créditos de escola
 *
 * Lista os créditos da escola
 *
  */
include_once 'ConnectPDO.class.php';

class CreditosDAO extends Connect{
	
	function __construct(){
				
	}


	
	//recupera códigos da secretaria e do governo de escolas do AB
	public function buscaGoverno($codSgeEscola){
		$CM_dados = array();
		$sql = "select CodSecretaria, CodGoverno, CodEscola from dbo.ESCOLA_GOVERNO where CodEscola= {$codSgeEscola}  or CodSecretaria= {$codSgeEscola}  or CodGoverno= {$codSgeEscola} ;";
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;
	}
	
	//verifica qual código 
	public function buscaEscolaPedido($Esc, $Gov, $Sec){
		$CM_dados = array();
		$sql      .= "select top 1 CodSGE from dbo.PEDIDOS_DIGITAIS where CodSGE in (" . $Esc ."," . $Gov ."," . $Sec . ")";
		$sql      .= " union ";
		$sql      .=  "select top 1 CodSGE from dbo.PEDIDOS_DIGITAIS_WEBTESTE where CodSGE in (" . $Esc ."," . $Gov ."," . $Sec . ");";
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;
	}

	//verifica se a escola existe, para inserção de créditos avulsos
	public function buscaEscolaPedidoSPE($Esc){
		$CM_dados = array();
		$sql = " select top 1 CodSGE from dbo.PEDIDOS_DIGITAIS where CodSGE in (" . $Esc . ") ";
		$sql .= " union ";
		$sql .= " select top 1 CodSGE from dbo.PEDIDOS_DIGITAIS_WEBTESTE where CodSGE in (" . $Esc . ");";		
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;
	}	
	
	//recupera a lista de créditos Portal
	// PARA ALUNOS - soma os produtos adquiridos, sem considerar disciplina
	// PARA PROFESSOR - Calcula o total adquirido pelo número de disciplinas que compõe cada produto
	public function buscaCreditosClass($codSgeEscola){
		$CM_dados = array();
		$sql = "SP_ListaCreditos {$codSgeEscola};";
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;		
	}

	public function buscaCreditosClassWebTest($codSgeEscola){
		$CM_dados = array();
		$sql = "SP_ListaCreditosWebTeste {$codSgeEscola};";
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;		
	}

	
	//Busca Créditos Avulsos Atuais
	public function buscaCreditosAvulsos($XXsge , $XXcodProduto , $XXnivelEnsino , $XXtipocliente ){
		$CM_dados = array();
		$sql = "select 
			COALESCE( (select sum(total) 
				from dbo.creditosAvulsos
				where ano=year(getdate())
				and codsge=" . $XXsge . "
				and sentido='C'
				and produto_idproduto=" . $XXcodProduto . "
				and nivelensino_idnivelensino=" . $XXnivelEnsino . "
				and tipousuario_idtipousuario=" . $XXtipocliente . "),0) - 
			COALESCE( (select sum(total) 
				from dbo.creditosAvulsos
				where ano=year(getdate())
				and codsge=" . $XXsge . "
				and sentido='D'
				and produto_idproduto=" . $XXcodProduto . "
				and nivelensino_idnivelensino=" . $XXnivelEnsino . "
				and tipousuario_idtipousuario=" . $XXtipocliente . ") ,0) as intQtde";
		$CM_dados = $this->execQuerySelect($sql);
		return $CM_dados;		
	}

	
	//Inserção de registro de crédito ou débito
	public function insereCreditos($XXsge , $XXcodProduto , $XXnivelEnsino , $XXtotal , $XXtipocliente , $XXoperacao){
		$CM_dados = array();
		$sql = "insert into dbo.creditosAvulsos (codsge, ano, produto_idproduto, ";
		if ($XXnivelEnsino!="") {
			$sql .= "nivelensino_idnivelensino,";
		}
		$sql .= "total, tipousuario_idtipousuario ,sentido) values (";
		$sql .= $XXsge . ",";
		$sql .= "year(getdate()),";
		$sql .= $XXcodProduto . ",";
		if ($XXnivelEnsino!="") {
			$sql .= $XXnivelEnsino . ",";
		}
		$sql .= $XXtotal . ",";
		$sql .= $XXtipocliente . ",";
		$sql .= "'".$XXoperacao."'";
		$sql .= ");";
		$CM_dados = $this->execQueryInsert($sql);
		// CM_dados[0] = true ou false
		// CM_dados[1] = mensagem de erro
		return $CM_dados;		
	}


}

?>