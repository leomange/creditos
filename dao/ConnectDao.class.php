<?php

/**
 * API de créditos de escola
 *
 * Lista os créditos da escola
 *
  */


class ConnectDAO{
	
	function __construct(){
		
		
	}

	#abre conexão com a base livros 
	public function UsePrd2015(){

		try{

			// $conn = mssql_connect('bded-dev.positivo', 'sisLivroDigital', 'RTEPVI2A');
			$conn = mssql_connect('bded-prd.positivo', 'sisLivroDigital', 'CP9D9VS5');
			mssql_select_db('LivroDigital',$conn);

			if($conn){

				return $conn;	

			}else{

				throw new ErrorException("UsePrd: Erro na conexão Prd.",115);
				
			}

			
		}catch(ErrorException $e){
			
			return false;

		}
	
	}
	
	
	#abre conexão com a base livros 
	public function UsePrd2014(){

		try{

			// $conn = mssql_connect('bded-dev.positivo', 'sisLivroDigital', 'RTEPVI2A');
			$conn = mssql_connect('bded-prd.positivo', 'sisLivroDigital', 'CP9D9VS5');
			mssql_select_db('LivrosDigitais',$conn);

			if($conn){

				return $conn;	

			}else{

				throw new ErrorException("UsePrd: Erro na conexão Prd.",115);
				
			}

			
		}catch(ErrorException $e){
			

			return false;

		}
	
	}

	
	
	public function execQuery($query){

		$CM_dados = array();
		
		try{

			$conn = $this->UsePrd2015();			
			$sql = $query;
			$result = mssql_query($sql,$conn);

			if(mssql_num_rows($result)){
				$row = mssql_fetch_array($result);				
				do{				
					$CM_dados = $row;				
				} while ($row = mssql_fetch_array($result));				
				return $CM_dados;
			}else{				
				throw new ErrorException("execQuery: Falhana consulta.(".$query.")",3326);

			}

			
		}catch(ErrorException $e){
			

			return false;

		}
	
	}
	


}