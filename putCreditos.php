<?php

include_once 'dao/TokenDAO.class.php';		//Objeto TOKEN
include_once 'dao/CreditosDAO.class.php';	//Objeto CREDITOS
include_once 'dao/SistemaDAO.class.php';	//Objeto SISTEMA






/* ENTRADA */
//--variaveis de parametro --
// SQL Injection - 1º tratamento com expressão regular
$Xauth = preg_replace('/[^[:alnum:]_]/', '', $_POST["auth"] );				//token de autorização - hash
$Xsge = preg_replace('/[^(0-9)_]/', '', $_POST["codsge"] );					//código sge da escola - deve ser número
$XcodProduto = preg_replace('/[^(0-9)_]/', '', $_POST["codProduto"] );		//produto 1-SGE ou 2-AB
$XnivelEnsino =  preg_replace('/[^(0-9)_]/', '', $_POST["nivelEnsino"] );	//1 EI // 2 - EF1 // 3 - EF2 // 4 - EM
$Xtotal =  preg_replace('/[^(0-9)_]/', '', $_POST["total"] );				//total de créditos a serem acrescidos ou debitados
$Xtipocliente =  preg_replace('/[^(0-9)_]/', '', $_POST["tipocliente"] );	//1 - ALUNOS // 2 - PROFESSORES
$Xoperacao = preg_replace('/[^(C|D)_]/', '', $_POST["operacao"] );			//sentido da operação C ou D









/*VALIDAÇÕES */
$erro = false; 	//flag - inicia em falso (sem erros); em caso de erro durante o processamento, recebe true. 
				//	 		Se estiver true, não executa o processamento imprime o erro na saída
$codErro = 0;	//registra o código de erro durante o processamento, para melhor identificação de falhas pelo cliente.

if (isset($Xauth)){		//verificamos se o parametro foi passadoe se não está em branco
	if ( ($Xauth=="") ) { $erro = true; $codErro="401.1";}
} else  {	$erro = true; $codErro="401.2";}

if (isset($Xsge)){		//verificamos se o parametro foi passadoe se não está em branco
	if ( ($Xsge=="") ) { $erro = true; $codErro="402.1";}
	if ( !(is_numeric($Xsge)) ) { $erro = true; $codErro="402.2"; }	//verificamos se é numérico para prever falha em banco de dados	
	if ( (strlen((string)$Xsge)>10) ) { $erro=true; $codErro="402.3"; }
} else { $erro = true; $codErro="402.4"; }

if (isset($XcodProduto)){ //verificamos se o parametro foi passadoe se não está em branco
	if ( $XcodProduto=="" ) { $erro = true; $codErro="403.1";}
	if ( !(is_numeric($XcodProduto)) ) { $erro = true; $codErro="403.2"; }	//verificamos se é numérico para prever falha em banco de dados	
	if ( (strlen((string)$XcodProduto)>1) ) { $erro=true; $codErro="403.3"; }
	$autorizados = "1,2";
	if (strrpos($autorizados, $XcodProduto ) === false) { // três sinais iguais = logicamente igual
		$erro=true; $codErro="403.4";
	}
} else { $erro = true; $codErro="403.5"; }

if (isset($XnivelEnsino)){ //verificamos se o parametro foi passadoe se não está em branco
	//if ( $XnivelEnsino=="" ) { $erro = true; $codErro="404.1";}	
	if ( $XnivelEnsino!=""){
		if ( !(is_numeric($XnivelEnsino)) ) { $erro = true; $codErro="404.2"; }	//verificamos se é numérico para prever falha em banco de dados	
		if ( (strlen((string)$XnivelEnsino)>1) ) { $erro=true; $codErro="404.3"; }
		$autorizados = "1,2,3,4";
		if (strrpos($autorizados, $XnivelEnsino ) === false) { // três sinais iguais = logicamente igual
			$erro=true; $codErro="404.4";
		}
	}
} else { 
	$XnivelEnsino="";
	//$erro = true; $codErro="404.5"; 
}

if (isset($Xtotal)){ //verificamos se o parametro foi passadoe se não está em branco
	if ( $Xtotal=="" ) { $erro = true; $codErro="405.1";}
	if ( !(is_numeric($Xtotal)) ) { $erro = true; $codErro="405.2"; }	//verificamos se é numérico para prever falha em banco de dados	
	if ( (strlen((string)$Xtotal)>6) ) { $erro=true; $codErro="405.3"; }
} else { $erro = true; $codErro="405.4"; }

if (isset($Xtipocliente)){ //verificamos se o parametro foi passadoe se não está em branco
	if ( $Xtipocliente=="" ) { $erro = true; $codErro="406.1";}
	if ( !(is_numeric($Xtipocliente)) ) { $erro = true; $codErro="406.2"; }	//verificamos se é numérico para prever falha em banco de dados	
	if ( (strlen((string)$Xtipocliente)>1) ) { $erro=true; $codErro="406.3"; }
	$autorizados = "1,2";
	if (strrpos($autorizados, $Xtipocliente ) === false) { // três sinais iguais = logicamente igual
		$erro=true; $codErro="406.4";
	}
} else { $erro = true; $codErro="406.5"; }

if (isset($Xoperacao)){ //verificamos se o parametro foi passadoe se não está em branco
	if ( $Xoperacao=="" ) { $erro = true; $codErro="407.1";}
	if ( (strlen((string)$Xoperacao)>1) ) { $erro=true; $codErro="407.2"; }
	$autorizados = "C,D";
	if (strrpos($autorizados, $Xoperacao ) === false) { // três sinais iguais = logicamente igual
		$erro=true; $codErro="407.3";
	}
} else { $erro = true; $codErro="407.4"; }












/* PROCESSAMENTO */	
if ( !($erro) ){	//se não deu erro, executa o processamento
		/*Objeto*/
		/* Validação do token pelo objeto TOKEN, método CURL (ainda não está em banco de dados) */
		$XXToken = new TokenDAO();
		$aux = $XXToken->validaTokenCurl($Xauth);
		$jsonToken = json_decode($aux, true);

		if ($jsonToken['status']){	//se token ok, status retorna true
			//buscaCreditos
			
			
			//o objeto CREDITOS retorna um array com os produtos e créditos da escola
			$auxResult = array();
			$XCreditos = new CreditosDAO($Xsge);
			
			
			//Verifica se a escola existe nas tabelas de pedido
			$auxEscola = array();


			// ===================================================================================
			// 2017/08/08 - Leonardo
			// 	Este bloco Não permite incluir créditos para escolas sem pedido.
			// 	Isso é errado para os casos de escolas onde o pedido é efetuado em outro código SGE.
			// 	Alterado para permitir a inclusão.
			// ===================================================================================
			/*
			$auxEscola = $XCreditos->buscaEscolaPedidoSPE($Xsge);

			if (count($auxEscola)>0){ 
				//se retornou algum registro, achou ao menos um pedido							
				if ($Xoperacao=="D"){ //se é débito, verifica se tem saldo
					$totalCreditos = array();
					$totalCreditos =  $XCreditos->buscaCreditosAvulsos($Xsge , $XcodProduto , $XnivelEnsino , $Xtipocliente );
					if ($Xtotal > $totalCreditos[0]['intQtde']){ //se está tentando debitar uma quantidade maior do que o crédito disponível na base
						$XXsistema = new SistemasDAO();
						echo $XXsistema->imprimeErro("410.1","Quantidade indisponível para esta operacao.");
						exit(0);
					}				
				}	
				$auxResult = $XCreditos->insereCreditos($Xsge , $XcodProduto , $XnivelEnsino , $Xtotal , $Xtipocliente , $Xoperacao) ;				
				
			} else {
				//não achou pedido para o código sge informado
				$XXsistema = new SistemasDAO();
				echo $XXsistema->imprimeErro("410.2","Escola não encontrada.");
				exit(0);
			}

			*/
			

			// ===================================================================================
			// 2017/08/08 - Leonardo
			// 	Este bloco permite incluir créditos para escolas sem pedido.
			// ===================================================================================
			if ($Xoperacao=="D"){ //se é débito, verifica se tem saldo
				$totalCreditos = array();
				$totalCreditos =  $XCreditos->buscaCreditosAvulsos($Xsge , $XcodProduto , $XnivelEnsino , $Xtipocliente );
				if ($Xtotal > $totalCreditos[0]['intQtde']){ //se está tentando debitar uma quantidade maior do que o crédito disponível na base
					$XXsistema = new SistemasDAO();
					echo $XXsistema->imprimeErro("410.1","Quantidade indisponível para esta operacao.");
					exit(0);
				}				
			}	
			$auxResult = $XCreditos->insereCreditos($Xsge , $XcodProduto , $XnivelEnsino , $Xtotal , $Xtipocliente , $Xoperacao) ;				
			// ===================================================================================

			
			

			
			/* saída Json */
			if ($auxResult[0]) {
				$saida = "{\"status\":\"true\",\"codigo\":\"200\"}";
				echo $saida;
			} else {
				$XXsistema = new SistemasDAO();
				echo $XXsistema->imprimeErro("409.1",$auxResult[1]);
			}
			exit(0);
				
		} else {
			
			//token retornou status false - token inválido
			//aciona o objeto SISTEMA para impressão do erro em JSON
			$XXsistema = new SistemasDAO();
			echo $XXsistema->imprimeErro("408.1","Token inválido.");
			exit(0);
		}
} else {
	//erro na validação dos parâmetros de entrada
	//aciona o objeto SISTEMA para impressão do erro em JSON	
	$XXsistema = new SistemasDAO();
	echo $XXsistema->imprimeErro($codErro ,"Parâmetros incorretos.");
	exit(0);	
}



?>