<?php
/*error_reporting(E_ALL);
ini_set("display_errors", 1);*/


include_once 'dao/TokenDAO.class.php';		//Objeto TOKEN
include_once 'dao/CreditosDAO.class.php';	//Objeto CREDITOS
include_once 'dao/SistemaDAO.class.php';	//Objeto SISTEMA

/* ENTRADA */
//--variaveis de parametro --
$Xauth = preg_replace('/[^[:alnum:]_]/', '', $_POST["auth"] );	//token de autorização - hash
$Xsge = preg_replace('/[^(0-9)_]/', '', $_POST["sge"] );		//código sge da escola - deve ser número


/*VALIDAÇÕES */
$erro = false; 	//flag - inicia em falso (sem erros); em caso de erro durante o processamento, recebe true.
				//	 		Se estiver true, não executa o processamento imprime o erro na saída
$codErro = 0;	//registra o código de erro durante o processamento, para melhor identificação de falhas pelo cliente.

if (isset($Xauth)){		//verificamos se o parametro foi passadoe se não está em branco
	if ( ($Xauth=="") ) { $erro = true; $codErro="401.1";}
} else  {	$erro = true; $codErro="401.2";}

if (isset($Xsge)){		//verificamos se o parametro foi passadoe se não está em branco
	if ( ($Xsge=="") ) { $erro = true; $codErro="401.3";}
	if ( !(is_numeric($Xsge)) ) { $erro = true; $codErro="401.4"; }	//verificamos se é numérico para prever falha em banco de dados
	if ( (strlen((string)$Xsge)>10) ) { $erro=true; $codErro="401.5"; }
} else { $erro = true; $codErro="401.6"; }


/* CABEÇALHO DE SAÍDA */
header('Content-Type: application/json; charset=utf-8');



/* PROCESSAMENTO */
if ( !($erro) ){	//se não deu erro, executa o processamento
		/*Objeto*/
		/* Validação do token pelo objeto TOKEN, método CURL (ainda não está em banco de dados) */
		$XXToken = new TokenDAO();
		$aux = $XXToken->validaTokenCurl($Xauth);
		$jsonToken = json_decode($aux, true);

		if ($jsonToken['status']){	//se token ok, status retorna true
			//buscaCreditos


			//o objeto CREDITOS retorna um array com os produtos e créditos da escola
			$auxArray = array();
			$XXCreditos = new CreditosDAO();

			$governo = array();
			$governo = $XXCreditos->buscaGoverno($Xsge);

			if (count($governo)>0){
				// se é AB

				//verificamos para quem o pedido foi emitido - secretaria, gverno ou escola
				//buscamos 1 pedido na base, o qual retornará qual o código usado na emissão do pedido
				$result = array();
				$result = $XXCreditos->buscaEscolaPedido($governo[0]['CodEscola'],$governo[0]['CodGoverno'],$governo[0]['CodSecretaria']);

				if (count($result)>0){
					// se achou algum pedido,  busca os créditos referentes ao código SGE retornado deste pedido encontrado
					$auxArray = $XXCreditos->buscaCreditosClass( $result[0]['CodSGE'] ) ;

					if (!count($auxArray)) {
						$auxArray = $XXCreditos->buscaCreditosClassWebTest( $result[0]['CodSGE'] ) ;
					}
				} else {
					//se não achou nenhum pedido, retorna vazio.
					$saida = "{\"status\":\"true\",\"codigo\":\"300\",\"checksum\":\"0\",\"content\":\"Vazio\"}";
					echo $saida;
					exit(0);
				}
			} else {
				// é SPE
				$auxArray = $XXCreditos->buscaCreditosClass($Xsge) ;

				if (!count($auxArray)) {
						$auxArray = $XXCreditos->buscaCreditosClassWebTest( $Xsge ) ;
				}
			}

			//echo(sizeof($auxArray));
			//exit(0);

			/* saída Json */
			if (count($auxArray)>0){
				/* Processamento deve ser campo a campo, não pode usar "json_encode" em virtude da conversão utf8 necessária para os campos texto retornados. */
				$saida = "{\"status\":\"true\",\"codigo\":\"200\",\"checksum\":\"####CS####\",\"content\":[";
				for ($g=0; $g<count($auxArray); $g++){

					$auxsaidaatual = "";
					$auxsaidaatual .= "\"intSge\":\"" . $auxArray[$g]["intSge"] . "\"";
					$auxsaidaatual .= ",\"strCodProduto\":\"" . $auxArray[$g]["strCodProduto"] . "\"";
					$auxsaidaatual .= ",\"strCodExterno\":\"" . $auxArray[$g]["strCodExterno"] . "\"";
					$auxsaidaatual .= ",\"strDescProduto\":\"" . utf8_encode($auxArray[$g]["strDescProduto"]) . "\"";
					$auxsaidaatual .= ",\"strSistema\":\"" . $auxArray[$g]["strSistema"] . "\"";
					$auxsaidaatual .= ",\"strAno\":\"" . utf8_encode($auxArray[$g]["strAno"]) . "\"";
					$auxsaidaatual .= ",\"strNivel\":\"" . utf8_encode($auxArray[$g]["strNivel"]) . "\"";
					$auxsaidaatual .= ",\"intQtde\":\"" . $auxArray[$g]["intQtde"] . "\"";
					$auxsaidaatual .= ",\"strTipo\":\"" . utf8_encode($auxArray[$g]["strTipo"]) . "\"";
					$auxsaidaatual .= ",\"intVolume\":\"" . utf8_encode($auxArray[$g]["intVolume"]) . "\"";
					$auxsaidaatual .= ",\"intPedido\":\"" . utf8_encode($auxArray[$g]["intPedido"]) . "\"";

					$saidaatual = "{\"checksum\":\"" . md5($auxsaidaatual). "\"," . $auxsaidaatual . "}";

					if ($g>0) $saida .= ",";
					$saida .= $saidaatual;
				}
				$saida .= "]}";
			} else {
				//se não achou nenhum crédito, retorna vazio.
				$saida = "{\"status\":\"true\",\"codigo\":\"300\",\"checksum\":\"0\",\"content\":\"Vazio\"}";
			}

			//impressão da saída e fim de processamento
			$saida = str_replace("####CS####", md5($saida) , $saida);
			echo $saida;
			exit(0);

		} else {

			//token retornou status false - token inválido
			//aciona o objeto SISTEMA para impressão do erro em JSON
			$XXsistema = new SistemasDAO();
			echo $XXsistema->imprimeErro("401.7","Token inválido.");
			exit(0);
		}
} else {
	//erro na validação dos parâmetros de entrada
	//aciona o objeto SISTEMA para impressão do erro em JSON
	$XXsistema = new SistemasDAO();
	echo $XXsistema->imprimeErro($codErro ,"Parâmetros incorretos.");
	exit(0);
}



?>
