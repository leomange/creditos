<?php

/**
 * API de créditos de escola
 *
 * Lista os créditos da escola
 *
  */
include_once 'ConnectPDO.class.php';

class TokenDAO extends Connect{
	
	function __construct(){
				
		$this->apiId = "CreditosAPI";
		
	}




	#valida o token
	#--------------------------------
	public function validaTokenCurl($token){
		
		$Xurl = "http://api.editorapositivo.com.br/tokensV2/verify/tokens/";
		
		$dados = array('apiid' => $this->apiId , 'token' => $token);
		
		//Abre a conexão para o método CURL
		$handle = curl_init($Xurl);
		if ($handle)
		{
			//Parametros do métofo CURL. Padrão ADOBE, não alteramos.
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($handle, CURLOPT_URL, $Xurl);
			curl_setopt($handle, CURLOPT_TIMEOUT, 30);
			
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $dados);
				
			//Envia a requisição CURL.
			$result = curl_exec($handle);
			
			//Encerra a conexão.
			curl_close($handle);
			
			if (!($result)){
					$success=false;
					return $success;
			} else {						
					$success=true;
					return $result;
			}
		}
		
		
	}
	
	
	
	#valida o token
	#--------------------------------
	public function getTokenCurl(){
		
		$Xurl = "http://api.editorapositivo.com.br/tokensV2/get/tokens/";
		
		//PositivoOn = a8440ca52c91453e39c625e3c7ba6c4e		
		$dados = array('appid' => 'a8440ca52c91453e39c625e3c7ba6c4e', 'operacao' => 'consulta', 'apiid' => $this->apiId );
		
		//Abre a conexão para o método CURL
		$handle = curl_init($Xurl);
		if ($handle)
		{
			//Parametros do métofo CURL. Padrão ADOBE, não alteramos.
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($handle, CURLOPT_URL, $Xurl);
			curl_setopt($handle, CURLOPT_TIMEOUT, 30);
			
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $dados);
				
			//Envia a requisição CURL.
			$result = curl_exec($handle);
			
			//Encerra a conexão.
			curl_close($handle);
			
			if (!($result)){
					$success=false;
					return $success;
			} else {						
					$success=true;
					return $result;
			}
		}
		
		
	}
	

}
?>